package com.techu.productos;

public class ProductoPrecioOnly {

    private long id;
    private double precio;

    public ProductoPrecioOnly() {
    }

    public ProductoPrecioOnly(double precio) {
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
