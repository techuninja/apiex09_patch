package com.techu.productos;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class ProductosController {

    public static List<Producto> productos = new ArrayList<>();

    /*@GetMapping("/v0/productos/{id}")
    public Producto getProductoId1(@PathVariable Integer id){
        Producto producto = new Producto(id);
        return producto;
    }

    @GetMapping("/v1/productos")
    public Producto getProductoId2(@RequestParam(value = "id", defaultValue = "0") Integer id){
        Producto producto = new Producto(id);
        return producto;
    }*/

    /*@DeleteMapping("/productos/{id}")
    public void deleteProducto(@PathVariable Integer id){
        //System.out.println(producto);
        //productos.add(producto);
    }*/

    @PatchMapping("/v0/productos/{id}")
    public Producto patchProductov0(@PathVariable long id, @RequestBody ProductoPrecioOnly proPrecio){
        Producto producto = new Producto(1);
        producto.setPrecio(proPrecio.getPrecio());
        return producto;
    }

    @PatchMapping("/v1/productos/{id}")
    public Producto patchProductov1(@PathVariable long id, @RequestBody Map<String, Object> updates){
        Producto producto = new Producto(1);
        if(updates.containsKey("precio")){
            producto.setPrecio((double) updates.get("precio"));
        }
        return producto;
    }
}
